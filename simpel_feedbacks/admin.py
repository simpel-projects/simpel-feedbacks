from django.contrib import admin

from .models import Feedback, Flag, Rating


class FlagAdmin(admin.ModelAdmin):
    pass


class RatingAdmin(admin.ModelAdmin):
    pass


class FeedbackAdmin(admin.ModelAdmin):
    pass


admin.site.register(Flag, FlagAdmin)
admin.site.register(Rating, RatingAdmin)
admin.site.register(Feedback, FeedbackAdmin)
